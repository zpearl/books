<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property integer $id
 * @property string $name
 *
 * @property BooksHasAuthors[] $booksHasAuthors
 * @property Books[] $books
 */
class Authors extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'   => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooksHasAuthors()
    {
        return $this->hasMany(BooksHasAuthors::className(), ['authors_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['id' => 'books_id'])->viaTable('books_has_authors',
                ['authors_id' => 'id']);
    }

    public function renderBooks()
    {

        $books = $this->getBooks()->orderBy('views ASC')->all();
        $out   = [];
        foreach ($books as $book) {
            $out[] = \yii\helpers\Html::a($book->title, ['/books/view', 'id' => $book->id]).', views: '.$book->views;
        }
        return implode('<br>', $out);
    }
}