<?php

namespace app\models;

use Yii;
use voskobovich\manytomany\ManyToManyBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $title
 * @property integer $year
 * @property string $annotation
 * @property string $category
 * @property integer $views
 *
 * @property BooksHasAuthors[] $booksHasAuthors
 * @property Authors[] $authors
 */
class Books extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'year', 'category'], 'required'],
            [['year', 'views'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['annotation'], 'string', 'max' => 120],
            [['category'], 'string', 'max' => 100],
            [['author_ids'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'title'      => 'Title',
            'year'       => 'Year',
            'annotation' => 'Annotation',
            'category'   => 'Category',
            'views'      => 'Views',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooksHasAuthors()
    {
        return $this->hasMany(BooksHasAuthors::className(), ['books_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthors()
    {
        return $this->hasMany(Authors::className(), ['id' => 'authors_id'])->viaTable('books_has_authors',
                ['books_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            [
                'class'     => ManyToManyBehavior::className(),
                'relations' => [
                    'author_ids' => 'authors',
                ],
            ],
        ];
    }
}