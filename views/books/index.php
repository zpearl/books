<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Yii::$app->user->isGuest ? '' : Html::a('Create Books', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'category',
            [
                'label'  => 'Authors',
//                'format' => 'raw',
                'value'  => function($data) {
//                    yii\helpers\VarDumper::dump($data->getAuthors()->asArray()->all(), 5, true);exit;
                    return implode(', ', ArrayHelper::map($data->authors, 'id', 'name'));
                },
            ],
            'year',
            'views',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
