<?php

use yii\db\Schema;
use yii\db\Migration;

class m160817_104415_Mass extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $transaction=$this->db->beginTransaction();
        try{
             $this->createTable('{{%authors}}',[
               'id'=> Schema::TYPE_PK,
               'name'=> Schema::TYPE_STRING."(255) DEFAULT NULL",
            ], $tableOptions);
             $this->createTable('{{%books}}',[
               'id'=> Schema::TYPE_PK,
               'title'=> Schema::TYPE_STRING."(255) NOT NULL",
               'year'=> Schema::TYPE_INTEGER."(11) NOT NULL",
               'annotation'=> Schema::TYPE_STRING."(120) DEFAULT NULL",
               'category'=> Schema::TYPE_STRING."(100) NOT NULL",
               'views'=> Schema::TYPE_INTEGER."(11) DEFAULT 0",
            ], $tableOptions);
             $this->createTable('{{%books_has_authors}}',[
               'books_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
               'authors_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
            ], $tableOptions);
            $this->createIndex('fk_books_has_authors_authors1_idx','{{%books_has_authors}}','authors_id',true);
            $this->createIndex('fk_books_has_authors_books1_idx','{{%books_has_authors}}','books_id',true);
             $this->createTable('{{%migration}}',[
               'version'=> Schema::TYPE_STRING."(180) NOT NULL",
               'apply_time'=> Schema::TYPE_INTEGER."(11) DEFAULT NULL",
            ], $tableOptions);
            $this->addForeignKey('fk_books_has_authors_authors_id','{{%books_has_authors}}','authors_id','authors', 'id');
            $this->addForeignKey('fk_books_has_authors_books_id','{{%books_has_authors}}','books_id','books', 'id');
            $transaction->commit();
        } catch (Exception $e) {
             echo 'Catch Exception '.$e->getMessage().' and rollBack this';
             $transaction->rollBack();
        }
    }

    public function down()
    {
        $transaction=$this->db->beginTransaction();
        try{
            $this->dropForeignKey('fk_books_has_authors_authors_id', '{{%books_has_authors}}');
            $this->dropForeignKey('fk_books_has_authors_books_id', '{{%books_has_authors}}');
            $this->dropTable('{{%authors}}');
            $this->dropTable('{{%books}}');
            $this->dropTable('{{%books_has_authors}}');
            $this->dropTable('{{%migration}}');
            $transaction->commit();
        } catch (Exception $e) {
            echo 'Catch Exception '.$e->getMessage().' and rollBack this';
            $transaction->rollBack();
        }
    }
}
